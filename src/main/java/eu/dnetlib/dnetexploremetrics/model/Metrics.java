package eu.dnetlib.dnetexploremetrics.model;

public class Metrics {

    //Records (Number of metadata collections registered/harvested by the OpenAIRE Research Graph)
    public String records;

    //Publications (Number of total publications available through OpenAIRE)
    public String publications;

    //Datasets (Number of software available through EXPLORE)
    public String datasets;

    //Software (Number of software available through EXPLORE)
    public String software;

    //OtherReseachProducts (Number of other research products through EXPLORE)
    public String orp;

    //Funders (This number of funders on EXPLORE)
    public String funders;

    //Projects (Funded projects)
    public String projects;

    //Content Providers
    public String contentProviders;

    //Organizations
    public String organizations;
}
