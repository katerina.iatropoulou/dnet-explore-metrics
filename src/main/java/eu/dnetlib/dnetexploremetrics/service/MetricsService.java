package eu.dnetlib.dnetexploremetrics.service;

import eu.dnetlib.dnetexploremetrics.dao.MetricsDAO;
import eu.dnetlib.dnetexploremetrics.dao.MetricsFileDAO;
import eu.dnetlib.dnetexploremetrics.model.Metrics;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@EnableScheduling
@Service
public class MetricsService {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private MetricsDAO metricsDAO;

    @Value("${search.service.api.url:https://services.openaire.eu/search/v2/api/}")
    private String searchUrl;

    private final String recordsRequest = "results?size=0&format=json";
    private final String publicationsRequest = "publications?size=0&format=json";
    private final String datasetsRequest = "datasets?size=0&format=json";
    private final String softwareRequest = "software?size=0&format=json";
    private final String orpRequest = "other?size=0&format=json";
    private final String fundersRequest = "resources2?format=json&refine=true&fields=relfunder&type=results&page=0&size=0";
    private final String projectsRequest = "projects?size=0&format=json";
    private final String contentProvidersRequest = "datasources?size=0&format=json";
    private final String organizationsRequest = "resources2?query=((reldatasourcecompatibilityid exact driver or " +
            "reldatasourcecompatibilityid exact driver-openaire2.0 or reldatasourcecompatibilityid exact openaire2.0 or " +
            "reldatasourcecompatibilityid exact openaire3.0 or reldatasourcecompatibilityid exact openaire4.0 or " +
            "reldatasourcecompatibilityid exact openaire-cris_1.1 or reldatasourcecompatibilityid exact openaire2.0_data" +
            " or reldatasourcecompatibilityid exact hostedBy or relproject=*))&type=organizations&size=0&format=json";

    @Scheduled(cron = "${calculation.time.cron}")
    private void calculateMetrics() {
        logger.info("Calculating metrics...");
        Metrics metrics = new Metrics();
        RestTemplate restTemplate  = new RestTemplate();

        ResponseEntity<String> response
                = restTemplate.getForEntity(searchUrl + recordsRequest, String.class);

        JSONObject jsonObject = new JSONObject(response.getBody());
        metrics.records= jsonObject.getJSONObject("meta").get("total").toString();

        response = restTemplate.
                getForEntity(searchUrl + publicationsRequest, String.class);
        jsonObject = new JSONObject(response.getBody());
        metrics.publications = jsonObject.getJSONObject("meta").get("total").toString();

        response = restTemplate.
                getForEntity(searchUrl + datasetsRequest, String.class);
        jsonObject = new JSONObject(response.getBody());
        metrics.datasets = jsonObject.getJSONObject("meta").get("total").toString();

        response = restTemplate.
                getForEntity(searchUrl + softwareRequest, String.class);
        jsonObject = new JSONObject(response.getBody());
        metrics.software = jsonObject.getJSONObject("meta").get("total").toString();

        response = restTemplate.
                getForEntity(searchUrl + orpRequest, String.class);
        jsonObject = new JSONObject(response.getBody());
        metrics.orp = jsonObject.getJSONObject("meta").get("total").toString();

        response = restTemplate.
                getForEntity(searchUrl + fundersRequest, String.class);
        jsonObject = new JSONObject(response.getBody());
        metrics.funders = jsonObject.getJSONObject("refineResults").getJSONArray("relfunder").length()+"";

        response = restTemplate.
                getForEntity(searchUrl + projectsRequest, String.class);
        jsonObject = new JSONObject(response.getBody());
        metrics.projects = jsonObject.getJSONObject("meta").get("total").toString();

        response = restTemplate.
                getForEntity(searchUrl + contentProvidersRequest, String.class);
        jsonObject = new JSONObject(response.getBody());
        metrics.contentProviders = jsonObject.getJSONObject("meta").get("total").toString();

        response = restTemplate.
                getForEntity(searchUrl + organizationsRequest, String.class);

        jsonObject = new JSONObject(response.getBody());
        metrics.organizations = jsonObject.getJSONObject("meta").get("total").toString();

        metricsDAO.save(metrics);
    }

    public String getMetrics() {
        return metricsDAO.getMetrics();
    }
}
