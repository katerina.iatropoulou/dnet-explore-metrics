package eu.dnetlib.dnetexploremetrics.utils;

import eu.dnetlib.dnetexploremetrics.model.Metrics;

public class PrometheusMetricsFormatter {

    private enum PrometheusMetrics {
        RECORDS("# TYPE explore_total_records gauge\nexplore_total_records %s"),
        PUBLICATIONS("# TYPE explore_records gauge\nexplore_records{type=\"publications\"} %s"),
        DATASETS("explore_records{type=\"datasets\"} %s"),
        SOFTWARE("explore_records{type=\"software\"} %s"),
        ORP("explore_records{type=\"otherresearchproducts\"} %s"),
        FUNDERS("# TYPE explore_total_funders gauge\nexplore_total_funders %s"),
        PROJECTS("# TYPE explore_total_projects gauge\nexplore_total_projects %s"),
        CONTENT_PROVIDERS("# TYPE explore_total_contentproviders gauge\nexplore_total_contentproviders %s"),
        ORGANIZATIONS("# TYPE explore_total_organizations gauge\nexplore_total_organizations %s\n");

        private String value;
        PrometheusMetrics(String value) {
            this.value = value;
        }
        private String getValue() {
            return value;
        }
    }

    public static String formatMetrics(Metrics metrics) {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format(String.valueOf(PrometheusMetrics.RECORDS.getValue()), metrics.records)).append("\n").
                append(String.format(String.valueOf(PrometheusMetrics.PUBLICATIONS.getValue()), metrics.publications)).append("\n").
                append(String.format(String.valueOf(PrometheusMetrics.DATASETS.getValue()), metrics.datasets)).append("\n").
                append(String.format(String.valueOf(PrometheusMetrics.SOFTWARE.getValue()), metrics.software)).append("\n").
                append(String.format(String.valueOf(PrometheusMetrics.ORP.getValue()), metrics.orp)).append("\n").
                append(String.format(String.valueOf(PrometheusMetrics.FUNDERS.getValue()), metrics.funders)).append("\n").
                append(String.format(String.valueOf(PrometheusMetrics.PROJECTS.getValue()), metrics.projects)).append("\n").
                append(String.format(String.valueOf(PrometheusMetrics.CONTENT_PROVIDERS.getValue()), metrics.contentProviders)).append("\n").
                append(String.format(String.valueOf(PrometheusMetrics.ORGANIZATIONS.getValue()),metrics.organizations));
        return builder.toString();
    }
}
