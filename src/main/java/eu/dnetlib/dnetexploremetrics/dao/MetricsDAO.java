package eu.dnetlib.dnetexploremetrics.dao;

import eu.dnetlib.dnetexploremetrics.model.Metrics;

import java.util.List;

public interface MetricsDAO {

    public void save(Metrics metrics);

    public String getMetrics();
}
