package eu.dnetlib.dnetexploremetrics.dao;

import eu.dnetlib.dnetexploremetrics.model.Metrics;
import eu.dnetlib.dnetexploremetrics.utils.PrometheusMetricsFormatter;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class MetricsFileDAO implements MetricsDAO {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Value("${filepath:/var/lib/tomcat_dnet/8980/webapps}")
    private String filePath;

    @Override
    public void save(Metrics metrics) {
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(filePath + "/metrics.txt");

            FileWriter fileWriter = new FileWriter(file);
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.print(PrometheusMetricsFormatter.formatMetrics(metrics));
            printWriter.close();

        } catch (IOException ioe) {
            logger.error("Error writing metrics to file " + filePath + "/metrics.txt.", ioe);
        }

    }

    @Override
    public String getMetrics() {
        String metrics="";
        try {
            FileInputStream inputStream = new FileInputStream(filePath +"/metrics.txt");
            metrics = IOUtils.toString(inputStream, "UTF-8");
            // do something with everything string
        } catch (IOException ioe) {
            logger.error("Error reading metrics to file " + filePath + "/metrics.txt.", ioe);
        }

        return metrics;
    }
}
