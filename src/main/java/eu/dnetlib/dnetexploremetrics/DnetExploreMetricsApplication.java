package eu.dnetlib.dnetexploremetrics;

import eu.dnetlib.dnetexploremetrics.dao.MetricsDAO;
import eu.dnetlib.dnetexploremetrics.dao.MetricsFileDAO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DnetExploreMetricsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DnetExploreMetricsApplication.class, args);
	}

	@Bean
	public MetricsDAO metricsDAO() {
		return new MetricsFileDAO();
	}

}
