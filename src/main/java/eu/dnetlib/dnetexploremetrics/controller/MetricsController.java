package eu.dnetlib.dnetexploremetrics.controller;

import eu.dnetlib.dnetexploremetrics.service.MetricsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MetricsController {

    @Autowired
    MetricsService metricsService;

    @RequestMapping(value = "/metrics", method = RequestMethod.GET, produces={"text/plain"})
    public String getMetrics() {
        return metricsService.getMetrics();
    }
}
